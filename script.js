let possibilidades = ['pedra', 'papel', 'tesoura', 'lagarto', 'spock']
let escolhaMaquina = possibilidades[(Math.floor(Math.random() * (4 -0 +0) + 0))];
let vitória = document.getElementById("vitoria");
let derrota = document.getElementById("derrota");
let empate = document.getElementById("empate");

console.log(escolhaMaquina);

document.getElementById("pedra").onclick = function () {
    if (escolhaMaquina === possibilidades[0]) {
        empate.classList.remove("hidden");
    }else if (escolhaMaquina === possibilidades[2] || escolhaMaquina === possibilidades[3]){
        vitória.classList.remove("hidden");
    }else {
        derrota.classList.remove("hidden");
    }window.scrollTo(0, 0);
}

document.getElementById("papel").onclick = function () {
    if (escolhaMaquina === possibilidades[1]) {
        empate.classList.remove("hidden");
    }else if (escolhaMaquina === possibilidades[0] || escolhaMaquina === possibilidades[4]){
        vitória.classList.remove("hidden");
    }else {
        derrota.classList.remove("hidden");
    }window.scrollTo(0, 0);
}

document.getElementById("tesoura").onclick = function () {
    if (escolhaMaquina === possibilidades[2]) {
        empate.classList.remove("hidden");
    }else if (escolhaMaquina === possibilidades[1] || escolhaMaquina === possibilidades[3]){
        vitória.classList.remove("hidden");
    }else {
        derrota.classList.remove("hidden");
    }window.scrollTo(0, 0);
}

document.getElementById("lagarto").onclick = function () {
    if (escolhaMaquina === possibilidades[3]) {
        empate.classList.remove("hidden");
    }else if (escolhaMaquina === possibilidades[5] || escolhaMaquina === possibilidades[1]){
        vitória.classList.remove("hidden");
    }else {
        derrota.classList.remove("hidden");
    }window.scrollTo(0, 0);
}

document.getElementById("spock").onclick = function () {
    if (escolhaMaquina === possibilidades[4]) {
        empate.classList.remove("hidden");
    }else if (escolhaMaquina === possibilidades[2] || escolhaMaquina === possibilidades[0]){
        vitória.classList.remove("hidden");
    }else {
        derrota.classList.remove("hidden");
    }window.scrollTo(0, 0);
}
document.getElementById("botao_reiniciar").onclick = function () {
    window.location.reload();
    window.scrollTo(0, 0);
}